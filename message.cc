#include "message.hh"
#include <iostream>

void Message::setMessage(const QString &message) {
    if(message != message_) {
        message_ = message;
        emit messageChanged();
    }
}

QString Message::message() const {
    return message_;
}

Q_INVOKABLE void Message::consoleLogMessage(const QString &message) {
    std::cout << "We have received the following message from the UI: " << message.toStdString() << std::endl;
}

Q_INVOKABLE int Message::getANumber() {
    return 2;
}

Q_INVOKABLE QString Message::getAString() {
    return QString("stuff");
}