// ui.qml
import QtQuick 2.0
import QtQuick.Controls 2.15

Item {
    property int stuff: msg.getANumber()
    property string initialMessage: msg.getAString()
    width: 300
    height: 100

    Text {
        x: 0; y: 0; width: 100; height: 100
        id: mainText
        text: msg.message == "" ? initialMessage : msg.message
    }
    Button {
        x: 0; y: 100; width: 100; height: 100
        id: aButton
        text: "A"
        onClicked: {
            var message = "The user clicked on button %1";
            msg.consoleLogMessage(message.arg(aButton.text));
            msg.message = aButton.text;
        }
    }
    Button {
        x: 0; y: 200; width: 100; height: 100
        id: bButton
        text: "B"
        onClicked: {
            var message = "The user clicked on button %1";
            msg.consoleLogMessage(message.arg(bButton.text));
            msg.message = bButton.text;
        }
    }
}