#include <QObject>
#include <QString>

class Message : public QObject {
        Q_OBJECT
        Q_PROPERTY(QString message READ message WRITE setMessage NOTIFY messageChanged)
    public:
        void setMessage(const QString &message);
        QString message() const;
        Q_INVOKABLE void consoleLogMessage(const QString &message);
        Q_INVOKABLE int getANumber();
        Q_INVOKABLE QString getAString();
    signals:
        void messageChanged();
    private:
        QString message_;
};