# Purpose

I'm trying to expose a property from the C++ side to the qml side of the application.

I'm following [this](https://doc.qt.io/qt-5/qtqml-cppintegration-exposecppattributes.html) tutorial more or less.

# Compilate and run

Just issue `qmake && make && ./expose_properties_to_qml`

# What does it do?

It displays a text box and 2 buttons. When you press a button we set the content of the text to the label of the button. But not directly. We use the setter method of the QObject exposed to the qml file and the QObject notifies the text element that the value has changed to it should change its value.

Also we log user actions by making the `consoleLogMessage` message available for the UI by tagging it `Q_INVOKABLE`.

# Notes

I still don't know whether it's possible to expose more complex object 