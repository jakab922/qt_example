#include <QGuiApplication>
#include <QQuickView>
#include <QUrl>
#include <QQmlEngine>
#include <QQmlContext> 
#include "message.hh"

int main(int argc, char **argv) {
    QGuiApplication app(argc, argv);

    QQuickView view;
    Message msg;
    view.engine()->rootContext()->setContextProperty("msg", &msg);
    view.setSource(QUrl::fromLocalFile("ui.qml"));
    view.show();

    return app.exec();
}